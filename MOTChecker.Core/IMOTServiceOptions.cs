﻿namespace MOTChecker.Core
{
    public interface IMOTServiceOptions
    {
        public string ApiKey { get; }
        public string ApiUrl { get; }
    }
}