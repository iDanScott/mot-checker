﻿using MOTChecker.Core.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MOTChecker.Core
{
    public class MOTService : IMOTService
    {
        private string _apiKey { get; }
        private string _apiUrl { get; }

        private ILogger _logger;

        public MOTService(IMOTServiceOptions opts, ILogger logger)
        {
            _apiKey = opts.ApiKey;
            _apiUrl = opts.ApiUrl;
            _logger = logger;
        }

        public async Task<List<VehicleDetails>> GetMOTDetails(string registration)
        {
            var client = new RestClient(_apiUrl);
            var request = new RestRequest("trade/vehicles/mot-tests")
                .AddParameter("registration", registration)
                .AddHeader("Accept", "application/json+v6")
                .AddHeader("x-api-key", _apiKey);

            var response = await client.ExecuteAsync<List<VehicleDetails>>(request).ConfigureAwait(false);

            if (Globals.Debug)
            {
                var parameters = string.Join("&", request.Parameters.Where(x => x.Type == ParameterType.GetOrPost).Select(x => $"{x.Name}={x.Value}"));
                var headers = string.Join(Environment.NewLine, request.Parameters.Where(x => x.Type == ParameterType.HttpHeader).Select(x => $"{x.Name}: {x.Value}"));
                _logger.Log($"[Debug] Request: [{request.Method}] {_apiUrl}/{request.Resource}?{parameters}{Environment.NewLine}HEADERS:{Environment.NewLine}{headers}");
                _logger.Log($"[Debug] Response Content: {response.Content}");
            }

            return response.Data;
        }
    }
}
