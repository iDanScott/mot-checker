﻿using System;

namespace MOTChecker.Core
{
    public class Logger : ILogger
    {
        public void Log(string message)
        {
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Write(message);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();
        }
    }
}
