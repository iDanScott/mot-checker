﻿using MOTChecker.Core.Models;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MOTChecker.Core
{
    public interface IMOTService
    {
        public Task<List<VehicleDetails>> GetMOTDetails(string registration);
    }
}
