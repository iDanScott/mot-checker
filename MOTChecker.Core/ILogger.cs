﻿namespace MOTChecker.Core
{
    public interface ILogger
    {
        void Log(string message);
    }
}
