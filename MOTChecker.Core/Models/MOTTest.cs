﻿using RestSharp;
using System.Text.Json.Serialization;

namespace MOTChecker.Core.Models
{
    public class MOTTest
    {
        [JsonPropertyName("completedDate")]
        public string CompletedDate { get; set; }

        [JsonPropertyName("testResult")]
        public string TestResult { get; set; }

        [JsonPropertyName("expiryDate")]
        public string ExpiryDate { get; set; }

        [JsonPropertyName("odometerValue")]
        public string OdometerValue { get; set; }

        [JsonPropertyName("odometerUnit")]
        public string OdometerUnit { get; set; }

        [JsonPropertyName("motTestNumber")]
        public string MotTestNumber { get; set; }

        [JsonPropertyName("odometerResultType")]
        public string OdometerResultType { get; set; }

        [JsonPropertyName("rfrAndComments")]
        public JsonArray RfrAndComments { get; set; }
    }
}