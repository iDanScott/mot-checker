﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace MOTChecker.Core.Models
{
    public class VehicleDetails
    {
        [JsonPropertyName("registration")]
        public string Registration { get; set; }

        [JsonPropertyName("make")]
        public string Make { get; set; }

        [JsonPropertyName("model")]
        public string Model { get; set; }

        [JsonPropertyName("firstUsedDate")]
        public string FirstUsedDate { get; set; }

        [JsonPropertyName("fuelType")]
        public string FuelType { get; set; }

        [JsonPropertyName("primaryColour")]
        public string PrimaryColour { get; set; }

        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; set; }

        [JsonPropertyName("registrationDate")]
        public string RegistrationDate { get; set; }

        [JsonPropertyName("manufacturedDate")]
        public string ManufactureDate { get; set; }

        [JsonPropertyName("engineSize")]
        public string EngineSize { get; set; }

        [JsonPropertyName("motTests")]
        public List<MOTTest> MOTTests { get; set; }

        public VehicleDetails()
        { }
    }
}