﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MOTChecker.Core;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace MOTChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddUserSecrets<Program>();
            
            Globals.Configuration = builder.Build();

            if (string.IsNullOrWhiteSpace(Globals.Configuration[nameof(MOTServiceOptions.ApiKey)]) || string.IsNullOrWhiteSpace(Globals.Configuration[nameof(MOTServiceOptions.ApiUrl)]))
            {
                throw new Exception("Both the API Key and the API URL endpoint must be specified in the appsettings.json or within user secrets for this application to run.");
            }

            Globals.Debug = args.Any(x => x == "-d");

            IServiceCollection services = new ServiceCollection()
                .AddSingleton<IMOTServiceOptions, MOTServiceOptions>()
                .AddSingleton<ILogger, Logger>()
                .AddSingleton<IMOTService, MOTService>();

            Handler(services.BuildServiceProvider());
        }

        private static void Handler(ServiceProvider serviceProvider)
        {
            var exit = false;
            do
            {
                Console.Write("Please enter a vehicle registration: ");
                var registration = Console.ReadLine();

                if (Regex.IsMatch(registration, Globals.RegistrationRegex))
                {
                    var response = serviceProvider.GetService<IMOTService>().GetMOTDetails(registration).Result;

                    foreach (var vehicleDetails in response)
                    {
                        Console.WriteLine($"Make: {vehicleDetails.Make}");
                        Console.WriteLine($"Model: {vehicleDetails.Model}");
                        Console.WriteLine($"Colour: {vehicleDetails.PrimaryColour}");

                        var lastMot = vehicleDetails.MOTTests.FirstOrDefault();

                        Console.WriteLine($"MOT Expiry Date: {lastMot?.ExpiryDate}");
                        Console.WriteLine($"Mileage at last MOT: {lastMot?.OdometerValue} {lastMot.OdometerUnit}");
                    }

                    Console.Write("Would you like to query another vehicle registration? (Y/n)?");
                    var another = Console.ReadKey(false).KeyChar;
                    Console.WriteLine();
                    exit = another == 'n';
                }
                else
                {
                    Console.WriteLine("The vehicle registration provided appears to be invalid. Please try again.");
                }
            } while (!exit);
        }
    }
}
