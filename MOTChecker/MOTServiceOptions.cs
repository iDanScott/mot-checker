﻿using MOTChecker.Core;

namespace MOTChecker
{
    public class MOTServiceOptions : IMOTServiceOptions
    {
        public string ApiKey => Globals.Configuration.GetSection(nameof(ApiKey)).Value;

        public string ApiUrl => Globals.Configuration.GetSection(nameof(ApiUrl)).Value;
    }
}